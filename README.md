![frankenburg-1918-960x200k.png](https://bitbucket.org/repo/8Gjapq/images/443892583-frankenburg-1918-960x200k.png)

# ImagingBook Source Code Repository

This repository contains **Java source code** accompanying the 
**digital image processing books** by **W. Burger and M. J. Burge**, 
published by Springer. This software is based on **[ImageJ](http://rsbweb.nih.gov/ij/index.html)**.
Please visit the main website **[imagingbook.com](https://imagingbook.com/)** for more information.

**Index terms:** digital image processing, computer algorithms, Java, ImageJ, textbook support.

##Source code

Source code is divided into multiple GIT repositories:

* **[imagingbook-common](https://bitbucket.org/imagingbook/imagingbook-common)** (imagingbook-common library)
* **[imagingbook_plugins_all](https://bitbucket.org/imagingbook/imagingbook-plugins-all/src)** (collection of all book plugins)
* **[imagingbook_plugins_en1](https://bitbucket.org/imagingbook/imagingbook-plugins-en1/src)** (English ‘Professional’ 1st ed, 2008)
* **[imagingbook_plugins_en2](https://bitbucket.org/imagingbook/imagingbook-plugins-en2/src)** (English ‘Undergraduate’ ed, Vols. 1-3, 2011-2013)
* **[imagingbook_plugins_en3](https://bitbucket.org/imagingbook/imagingbook-plugins-en3/src)** (English ‘Professional’ 2nd ed, hardcover, 2016)
* **[imagingbook_plugins_de2](https://bitbucket.org/imagingbook/imagingbook-plugins-de2/src)** (German 2nd ed., 2006)
* **[imagingbook_plugins_de3](https://bitbucket.org/imagingbook/imagingbook-plugins-de3/src)** (German 3rd ed., 2015)

The **complete source tree** may be obtained by cloning **this** repository, which contains
the above repositories as GIT submodules:

* **[imagingbook-public](https://bitbucket.org/imagingbook/imagingbook-public/src)** (clone recursively)


## Downloads

See [here](https://bitbucket.org/imagingbook/imagingbook-public/wiki/Installation%20and%20setup)
for instructions on installation and setup.

* [**JAR files** (common library and ImageJ plugins)](https://bitbucket.org/imagingbook/imagingbook-public/downloads)



##JavaDoc Pages (common library and plugins for different book editions)

* **[imagingbook-common](http://imagingbook.bitbucket.org/javadoc/imagingbook-common)** (imagingbook-common library)
* **[imagingbook_plugins_all](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_all)** (collection of all book plugins)
* **[imagingbook_plugins_en1](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_en1)** (English ‘Professional’ 1st ed, 2008)
* **[imagingbook_plugins_en2](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_en2)** (English ‘Undergraduate’ ed, Vols. 1-3, 2011-2013)
* **[imagingbook_plugins_en3](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_en3)** (English ‘Professional’ 2nd ed, hardcover, 2016)
* **[imagingbook_plugins_de2](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_de2)** (German 2nd ed., 2006)
* **[imagingbook_plugins_de3](http://imagingbook.bitbucket.org/javadoc/imagingbook_plugins_de3)** (German 3rd ed., 2015)

## Wiki

* [Overview](https://bitbucket.org/imagingbook/imagingbook-public/wiki/browse/)
* [Installation and setup](https://bitbucket.org/imagingbook/imagingbook-public/wiki/Installation%20and%20setup)
* [License](https://bitbucket.org/imagingbook/imagingbook-public/wiki/License)

## Note

This is the official **imagingbook** repository (previously hosted at http://sourceforge.net/projects/imagingbook/ until March 2016).

## Related projects

* **[imagingbook-calibrate](https://bitbucket.org/imagingbook/imagingbook-calibrate)** (camera calibration)
* **[imagingbook-violajones](https://bitbucket.org/imagingbook/imagingbook-violajones)** (face detection)
